FROM nginx:latest

LABEL author="Dalmiro Granas"

COPY ./.docker/config/nginx.custom.conf /etc/nginx/nginx.conf

RUN apt-get update
RUN apt-get -y install vim
RUN apt-get -y install lynx

EXPOSE 80

ENTRYPOINT ["nginx"]
CMD ["-g", "daemon off;"]