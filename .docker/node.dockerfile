FROM node:latest

LABEL author="Dalmiro Granas"

ENV MONGO_URL=mongodb://mongo:27017/dev

WORKDIR /var/www/NodeExpressApp

COPY ./package.json .

RUN npm install

COPY    . .

#EXPOSE 		3000

CMD [ "node","index.js" ]